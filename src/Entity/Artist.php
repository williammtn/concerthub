<?php

namespace App\Entity;

use App\Repository\ArtistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

#[ORM\Entity(repositoryClass: ArtistRepository::class)]
#[ORM\Table(name:"artist")]
class Artist
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 45)]
    private string $name;

    /**
     * @var Collection|PersistentCollection
     */
    #[ORM\OneToMany(mappedBy:"artist", targetEntity:"App\Entity\Event")]
    private PersistentCollection|Collection $events;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getEvents(): PersistentCollection|Collection
    {
        return $this->events;
    }

    public function setEvents(PersistentCollection|Collection $events): void
    {
        $this->events = $events;
    }
}
