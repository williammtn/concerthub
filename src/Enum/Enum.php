<?php

namespace App\Enum;

enum Enum
{
    const ID = 'id';
    const ARTIST = 'artist';
    const EVENT = 'event';
    const CITY = 'city';
    const DATE = 'date';
    CONST NAME = 'name';

}
