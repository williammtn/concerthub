<?php

namespace App\Controller;

use App\Enum\Enum;
use App\Repository\ArtistRepository;
use App\Repository\CityRepository;
use App\Repository\EventRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(EventRepository $eventRepository): Response
    {
        $query = $eventRepository->getAllEvents();

        foreach ($query as $event){
            $concerts[] = [
                'artist' => $event[Enum::ARTIST][Enum::NAME],
                'date' => ($event[Enum::DATE])->format('d/m/Y'),
                'city' => $event[Enum::CITY][Enum::NAME],
            ];
        }

        return $this->render('home/home.html.twig',[
            'concerts' => $concerts ?? [],
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/rechercher-concert', name: 'search_event')]
    public function search(EventRepository $eventRepository, Request $request, ArtistRepository $artistRepository, CityRepository $cityRepository): Response
    {
        $artist = $request->query->get('artist');
        $date1 = $request->query->get('date1');
        $date2 = $request->query->get('date2');
        $city = $request->query->get('city');

        $queryBuilder = $eventRepository->createQueryBuilder('e');

        if ($artist !== '') {
            $artistObject = $artistRepository->findOneBy(['name' => $artist]);
            if ($artistObject !== null) {
                $queryBuilder->andWhere('e.artist = :artist')
                    ->setParameter('artist', $artistObject);
            }
        }

        if ($date1 !== '') {
            $date1 = DateTime::createFromFormat('d/m/Y', $date1);
            if ($date1 !== false) {
                $queryBuilder->andWhere('e.date >= :startDate')
                    ->setParameter('startDate', $date1->format('Y-m-d'));
                if ($date2 !== '') {
                    $date2 = DateTime::createFromFormat('d/m/Y', $date2);
                    if ($date2 !== false) {
                        $queryBuilder->andWhere('e.date <= :endDate')
                            ->setParameter('endDate', $date2->format('Y-m-d'));
                    }
                }
                else{
                    $queryBuilder->andWhere('e.date <= :endDate')
                    ->setParameter('endDate', $date1->format('Y-m-d'));
                }
            }
        }

        if ($city !== '') {
            $cityObject = $cityRepository->findOneBy(['name' => $city]);
            if ($cityObject !== null) {
                $queryBuilder->andWhere('e.city = :city')
                    ->setParameter('city', $cityObject);
            }
        }
        $query = $queryBuilder->getQuery()->getResult();

        if($artist == '' && $date1 == '' && $city == '') {
            $query = $eventRepository->findAll();
        }

        $concerts = [];
        foreach ($query as $event) {
            $concerts[] = [
                'artist' => $event->getArtist()->getName(),
                'date' => $event->getDate()->format('d/m/Y'),
                'city' => $event->getCity()->getName(),
            ];
        }

        return $this->render('home/home.html.twig', [
            'concerts' => $concerts,
        ]);
    }

}