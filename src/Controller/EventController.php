<?php

namespace App\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends AbstractController
{
    #[Route('/concerts', name: 'event_page')]
    public function index(): Response
    {
        return $this->render('event/event.html.twig');
    }

    #[Route('/concerts/{id}', name: 'event')]
    public function eventPage(): Response
    {
        return $this->render('event/event.html.twig');
    }
}