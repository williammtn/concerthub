<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\City;
use App\Entity\Event;
use App\Enum\Enum;
use App\Repository\ArtistRepository;
use App\Repository\CityRepository;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Exception;

class AdminController extends AbstractController
{
    #[Route('/administration', name: 'admin')]
    public function index(EventRepository $eventRepository, ArtistRepository $artistRepository, CityRepository $cityRepository): Response
    {
        return $this->render('admin/admin.html.twig', [
            'concerts' => $eventRepository->findAll(),
            'artists' => $artistRepository->findAll(),
            'cities' => $cityRepository->findAll(),
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/administration/ajouter-concert', name: 'admin_add_event')]
    public function addEvent(Request $request, ArtistRepository $artistRepository, CityRepository $cityRepository, EntityManagerInterface $em): Response
    {
        $parameters = $request->request->all();
        $date = $parameters[Enum::DATE];
        $city = $parameters[Enum::CITY];
        $artist = $parameters[Enum::ARTIST];

        $artistObject = $artistRepository->findOneBy(['id' => $artist]);
        $cityObject = $cityRepository->findOneBy(['id' => $city]);

        $event = new Event();
        $event->setDate(DateTime::createFromFormat('d/m/Y', $date));
        $event->setCity($cityObject);
        $event->setArtist($artistObject);
        $em->persist($event);
        $em->flush();

        return $this->redirectToRoute('admin');
    }

    #[Route('/administration/editer-concert/{id}', name: 'admin_edit_event', requirements: ['id' => '\d+'])]
    public function editPage(EventRepository $eventRepository,ArtistRepository $artistRepository, CityRepository $cityRepository, int $id): Response
    {
        $event = $eventRepository->findOneBy(['id' => $id]);
        return $this->render('admin/edit_event.html.twig', [
            'concerts' => $eventRepository->findAll(),
            'artists' => $artistRepository->findAll(),
            'cities' => $cityRepository->findAll(),
            'concertToEdit' => $event,
        ]);
    }

    #[Route('/administration/edition/{id}', name: 'admin_update_event', requirements: ['id' => '\d+'])]
    public function edit(Request $request, EntityManagerInterface $em, ArtistRepository $artistRepository, CityRepository $cityRepository, int $id): Response
    {
        $parameters = $request->request->all();
        $date = $parameters[Enum::DATE];
        $city = $parameters[Enum::CITY];
        $artist = $parameters[Enum::ARTIST];
        $artistObject = $artistRepository->findOneBy(['id' => $artist]);
        $cityObject = $cityRepository->findOneBy(['id' => $city]);

        $event = $em->getRepository(Event::class)->findOneBy(['id' => $id]);
        $event->setDate(DateTime::createFromFormat('d/m/Y', $date));
        $event->setCity($cityObject);
        $event->setArtist($artistObject);
        $em->persist($event);
        $em->flush();
        return $this->redirectToRoute('admin');
    }

    #[Route('/administration/supprimer-concert/{id}', name: 'admin_delete_event', requirements: ['id' => '\d+'])]
    public function delete(EventRepository $eventRepository, EntityManagerInterface $em, int $id): Response
    {
        $eventToDelete = $eventRepository->findOneBy(['id' => $id]);
        $em->remove($eventToDelete);
        $em->flush();

        return $this->redirectToRoute('admin');
    }

    #[Route('/administration/ajouter-artiste', name: 'admin_add_artist')]
    public function addArtist(Request $request, EntityManagerInterface $em): Response
    {
        $parameters = $request->request->all();
        $artistName = $parameters[Enum::ARTIST];

        $artist = new Artist();
        $artist->setName($artistName);
        $em->persist($artist);
        $em->flush();

        return $this->redirectToRoute('admin');
    }

    #[Route('/administration/ajouter-ville', name: 'admin_add_city')]
    public function addCity(Request $request, EntityManagerInterface $em): Response
    {
        $parameters = $request->request->all();
        $cityName = $parameters[Enum::CITY];

        $city = new City();
        $city->setName($cityName);
        $em->persist($city);
        $em->flush();

        return $this->redirectToRoute('admin');
    }
}