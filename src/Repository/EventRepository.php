<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

/**
 * @extends ServiceEntityRepository<Event>
 *
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * Méthode permettant d'obtenir tous les évènements prévus sur les 3 prochains mois pour la page d'accueil
     * @return array
     */
    public function getAllEvents(): array
    {
        $q = $this->createQueryBuilder('events')
            ->select('e', 'artist', 'city')
            ->from('App\Entity\Event', 'e')
            ->leftJoin('e.artist', 'artist')
            ->leftJoin('e.city', 'city')
            ->where('e.date >= :today')
            ->andWhere('e.date <= :threeMonths')
            ->orderBy('e.date', 'ASC')
            ->setParameter('today', new DateTime('-1 days'))
            ->setParameter('threeMonths', new DateTime('+3 months'));

        return $q->getQuery()->getArrayResult();
    }

}
